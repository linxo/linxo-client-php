# Introduction

Provides a PHP Library for the current version of the Linxo API.
The goal of this library is to simplify the usage of the JSON Linxo API
for PHP Developers.

## Prerequisites
Make sure that you have the following information:
* a server domain. For instance, `partners.linxo.com` may be the server that was given 
  to perform your initial requests
* a set of `API_KEY` and `API_SECRET`, in order to authenticate to the server
* PHP equipped with curl and json
* Make sure that the user that runs the PHP script has write access to the `/tmp` folder. 
  This is the place where session data is stored.
  


## Installation

First clone the repository
```
git clone https://bitbucket.org/linxo/linxo-client-php.git`
```

Then add the `linxo-client-php` folder in the `include_path` of your `php.ini`.

You are now ready to go !

## Usage

You can have a quick look at the file `src/examples/BasicClient.php`. 
But what you have to remember is that you have to instanciate an object of the 
class `LinxoClient` with your credentials, and then use the method `sendAction`
to interact with the server.

## Examples

### Login

##### How to login a client

```php
$linxoClient = new LinxoClient("partners.linxo.com", "API KEY" , "API SECRET");
$linxoClient->initialize();
$loginResult = $linxoClient->sendAction(new LoginAction("user@domain.tld", "aPassword"));
if ( $loginResult->userId == NULL )
{
  die ("Error");
}
... continue with other actions
```
