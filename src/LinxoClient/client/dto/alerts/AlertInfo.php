<?php
/**
 *  Copyright (c) 2015-2015 Linxo, All Rights Reserved.
 *
 * COPYRIGHT:
 *      This software is the property of Linxo.
 *      It cannot be copied, used, or modified without obtaining an
 *      authorization from the authors or a person mandated by Linxo.
 *      If such an authorization is provided, any modified version
 *      or copy of the software has to contain this header.
 *
 * WARRANTIES:
 *      This software is made available by the authors in the hope
 *      that it will be useful, but without any warranty.
 *      Linxo is not liable for any consequence related to
 *      the use of the provided software.
 *
 * User: hugues
 * Date: 02/11/2015
 */

namespace LinxoClient\client\dto\alerts;


use LinxoClient\client\data\pfm\alert\AlertType;
use LinxoClient\client\dto\EntityInfo;

class AlertInfo extends EntityInfo
{

  const TRANSACTION_URL_FORMAT = "%1\$stransaction?id=%2\$s";
  const ACCOUNT_URL_FORMAT     = "%1\$transactions?accountId=%2\$s";
  const GROUP_URL_FORMAT       = "%1\$settings/accounts?groupId=%2\$s";


  /**
   * @param $mobileScheme
   * @param AlertType $type
   * @param $itemId
   * @return null|string
   */
  static function getItemUrl($mobileScheme, $type, $itemId) {

    switch($type) {
      // Transactions
      case AlertType::BankFee:
      case AlertType::HighDeposit:
      case AlertType::HighSpending:
      return sprintf(AlertInfo::TRANSACTION_URL_FORMAT, $mobileScheme, $itemId);

      // account
      case AlertType::LowBalance:
        return sprintf(AlertInfo::ACCOUNT_URL_FORMAT, $mobileScheme, $itemId);

      // group
      case AlertType::PasswordChangeRequired:
      case AlertType::TooManyAttempts:
      case AlertType::UserActionRequired:
      return sprintf(AlertInfo::GROUP_URL_FORMAT, $mobileScheme, $itemId);

      default:
        return null;
    }
  }

  var $postedDate;
  var $itemDate;
  var $itemUrl;
  var $type;
  var $htmlMessage;
  var $label;
  var $accountName;
  var $amount;
  var $categoryId;

  /**
   * AlertInfo constructor.
   * @param \stdClass $that
   */
  public function __construct(\stdClass $that)
  {
    parent::__construct($that);

    $this->accountName = $that->accountName;
    $this->amount = $that->amount;
    $this->categoryId = $that->categoryId;
    $this->htmlMessage = $that->htmlMessage;
    $this->itemDate = $that->itemDate;
    $this->label = $that->label;
    $this->postedDate = $that->postedDate;
    $this->type = $that->type;

    $this->itemUrl = ($that->itemUrl != null)
        ? $that->itemUrl
        : self::getItemUrl($that->mobileScheme, $that->type, $that->itemId);
  }


}