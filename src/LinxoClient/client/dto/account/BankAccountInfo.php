<?php
/**
 *  Copyright (c) 2015-2015 Linxo, All Rights Reserved.
 *
 * COPYRIGHT:
 *      This software is the property of Linxo.
 *      It cannot be copied, used, or modified without obtaining an
 *      authorization from the authors or a person mandated by Linxo.
 *      If such an authorization is provided, any modified version
 *      or copy of the software has to contain this header.
 *
 * WARRANTIES:
 *      This software is made available by the authors in the hope
 *      that it will be useful, but without any warranty.
 *      Linxo is not liable for any consequence related to
 *      the use of the provided software.
 *
 * User: hugues
 * Date: 26/10/2015
 */

namespace LinxoClient\client\dto\account;


require_once(__DIR__.'/../EntityInfo.php');
use LinxoClient\client\dto\EntityInfo;

require_once(__DIR__.'/../../actions/pfm/events/SynchronizationUpdateEvent.php');
use LinxoClient\clients\action\pfm\events\SynchronizationUpdateEvent;

class BankAccountInfo extends EntityInfo
{

  var $accountNumber;
  var $reference;
  var $name;
  var $currentBalance;
  var $accountGroupName;
  var $accountGroupId;

  /**
   * listed in LinxoClient\client\data\pfm\bank\AccountType
   */
  var $accountType;

  var $includedInTrends;
  var $creationDate;
  var $currentBalanceDate;
  var $closeDate;

  /**
   * listed in LinxoClient\client\data\pfm\bank\AccountSynchroStatus
   */
  var $status;

  /**
   * @type SynchronizationUpdateEvent
   */
  var $lastEvent;

  /**
   * BankAccountInfo constructor.
   */
  public function __construct(\stdClass $that)
  {
    parent::__construct($that);

    $this->accountNumber  = $that->accountNumber ;
    $this->reference = $that->reference;
    $this->name = $that->name;
    $this->currentBalance = $that->currentBalance;
    $this->accountGroupName = $that->accountGroupName;
    $this->accountGroupId = $that->accountGroupId;

    $this->accountType = $that->accountType;
    $this->includedInTrends = $that->includedInTrends;
    $this->creationDate = $that->creationDate;
    $this->currentBalanceDate = $that->currentBalanceDate;
    $this->closeDate = $that->closeDate;

    $this->status = $that->status;

    if($that->lastEvent != null){
      $this->lastEvent = new SynchronizationUpdateEvent($that->lastEvent);
    }
  }


}