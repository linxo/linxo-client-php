<?php
/**
 *  Copyright (c) 2015-2015 Linxo, All Rights Reserved.
 *
 * COPYRIGHT:
 *      This software is the property of Linxo.
 *      It cannot be copied, used, or modified without obtaining an
 *      authorization from the authors or a person mandated by Linxo.
 *      If such an authorization is provided, any modified version
 *      or copy of the software has to contain this header.
 *
 * WARRANTIES:
 *      This software is made available by the authors in the hope
 *      that it will be useful, but without any warranty.
 *      Linxo is not liable for any consequence related to
 *      the use of the provided software.
 *
 * User: hugues
 * Date: 26/10/2015
 */


namespace LinxoClient\client\dto\account;

require_once(__DIR__.'/../../../infrastructure/BasicEnum.php');
use LinxoClient\infrastructure\BasicEnum;

abstract class OperationUpdateInfoType extends BasicEnum
{
    const Started = 'Started'; // nothing
    const LoggingIn = 'LoggingIn'; // nothing
    const CredentialsValidated = 'CredentialsValidated'; // variable list of credential keys
    const LoggedIn = 'LoggedIn'; // nothing
    const ReadingAccountList = 'ReadingAccountList'; // nothing
    const ReadingAccount = 'ReadingAccount'; // {0} accountName, {1} currentPage (starts at 1), {2} count of transactions read until now (starts at 0)
    const FinishedAccount = 'FinishedAccount'; // {0} accountName, {1} count of read pages (starts at 0) {2} count of transactions read until now (starts at 0)
    const AccountFailure = 'AccountFailure'; // {0} accountName
    const AccountNotFound = 'AccountNotFound'; // {0} accountName
    const TransactionsRead = 'TransactionsRead'; // {0} accountName, {1} count of read pages (starts at 0) {2} count of transactions read until now
    const LoggingOut = 'LoggingOut'; // nothing

    /**
     * The job requires to enter a challenge response. Values would follow this format:
     * <ol>
     * <li>credentialKey1,
     * <li>credentialKey1.arg.arg1_name=arg1_value
     * <li>credentialKey1.arg.arg2_name=arg2_value
     * <li>credentialKey2,
     * <li>credentialKey2.arg.arg1_name=arg1_value
     * </ol>
     * where credentialKeyX are the requested credentials
     */
    const ChallengeRequested = 'ChallengeRequested';
    const NeedsSecret = 'NeedsSecret'; // the user needs to provide the secret to synchronize the group

}


require_once ( __DIR__. "/../EntityInfo.php");
use LinxoClient\client\dto\EntityInfo;

class OperationUpdateInfo extends EntityInfo
{
  /**
   * @type OperationUpdateInfoType
   */
  var $type;

  /** @array of parameter to the update */
  var $values;

  /**
   * OperationUpdateInfo constructor.
   */
  public function __construct(\stdClass $that)
  {
    parent::__construct($that);

    $this->type = OperationUpdateInfoType::valueOf($that->type);
    $this->value = $that->values;
  }


}
