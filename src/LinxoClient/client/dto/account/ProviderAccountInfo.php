<?php
/**
 *  Copyright (c) 2015-2015 Linxo, All Rights Reserved.
 *
 * COPYRIGHT:
 *      This software is the property of Linxo.
 *      It cannot be copied, used, or modified without obtaining an
 *      authorization from the authors or a person mandated by Linxo.
 *      If such an authorization is provided, any modified version
 *      or copy of the software has to contain this header.
 *
 * WARRANTIES:
 *      This software is made available by the authors in the hope
 *      that it will be useful, but without any warranty.
 *      Linxo is not liable for any consequence related to
 *      the use of the provided software.
 *
 * User: hugues
 * Date: 26/10/2015
 */


namespace LinxoClient\client\dto\account;

require_once(__DIR__.'/../../../infrastructure/BasicEnum.php');
use LinxoClient\infrastructure\BasicEnum;

abstract class Status extends BasicEnum
{
  const  Disappeared = 'Disappeared';
  const  NewAccount = 'New';
  const  Remains = 'Remains';
}


class ProviderAccountInfo
{
  var $uid;
  var $accountNumber;
  var $balance;
  var $balanceDate;

  /**
   * value in LinxoClient\client\data\pfm\bank\AccountType
   */
  var $type;

  /**
   * value in LinxoClient\client\dto\account\ProviderAccountInfo\Status
   */
  var $status;

  var $accountName;
}
