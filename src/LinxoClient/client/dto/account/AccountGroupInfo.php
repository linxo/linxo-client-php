<?php
/*
   Copyright (c) 2015-2015 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
 */
namespace LinxoClient\client\dto\account;

require_once ( __DIR__. "/../../data/pfm/group/SynchroStatus.php");

require_once ( __DIR__. "/../EntityInfo.php");
use LinxoClient\client\data\pfm\group\SynchroStatus;
use LinxoClient\client\dto\EntityInfo;

require_once ( __DIR__. "/../../actions/pfm/events/SynchronizationUpdateEvent.php");
use LinxoClient\clients\action\pfm\events\SynchronizationUpdateEvent;

require_once ( __DIR__. "/BankAccountInfo.php");

class AccountGroupInfo extends EntityInfo
{
  /**
   * @type string
   */
  var $name;

  /**
   * @type double
   */
  var $balance;

  /**
   * @type \LinxoClient\client\data\pfm\group\SynchroStatus
   */
  var $status;


  /**
   * @type string
   */
  var $url;

  /**
   * @type string
   */
  var $logoUrl;

  /**
   * @type date
   */
  var $lastSuccessfulUpdate;

  /**
   * @type date
   */
  var $lastUpdate;
  /**
   * @type date
   */
  var $lastStart;
  /**
   * @type date
   */
  var $lastSuccess;
  /**
   * @type date
   */
  var $lastEnd;

  /**
   * @type boolean
   */
  var $semiAuto;

  /**
   * @type boolean
   */
  var $unavailableMessage;

  /**
   * map : accountId =&gt; BankAccountInfo
   */
  var $bankAccounts;


  /**
   * @type long
   */
  var $fid;

  /**
   * @type SynchronizationUpdateEvent
   */
  var $lastEvent;

  /**
   * AccountGroupInfo constructor.
   * @param \stdClass $that
   */
  public function __construct(\stdClass $that)
  {
    parent::__construct($that);

    $this->name = $that->name;
    $this->balance = $that->balance;

    if($that->status != null) {
      $this->status = SynchroStatus::valueOf($that->status);
    }

    $this->url = $that->url;
    $this->logoUrl = $that->logoUrl;

    $this->lastSuccessfulUpdate = $that->lastSuccessfulUpdate;
    $this->lastUpdate = $that->lastUpdate;
    $this->lastStart = $that->lastStart;
    $this->lastSuccess= $that->lastSuccess;
    $this->lastEnd = $that->lastEnd;

    $this->semiAuto = $that->semiAuto;
    $this->unavailableMessage= $that->unavailableMessage;

    $this->fid = $that->fid;

    if($that->lastEvent != null){
      $this->lastEvent = new SynchronizationUpdateEvent($that->lastEvent);
    }

    $this->bankAccounts = array();
    foreach($that->bankAccounts as $accountId => &$stdBankAccount)
    {
      $this->bankAccounts[$accountId] = new BankAccountInfo($stdBankAccount);
    }
  }


}