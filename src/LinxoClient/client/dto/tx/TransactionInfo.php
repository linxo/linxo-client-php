<?php
/**
 *  Copyright (c) 2015-2015 Linxo, All Rights Reserved.
 *
 * COPYRIGHT:
 *      This software is the property of Linxo.
 *      It cannot be copied, used, or modified without obtaining an
 *      authorization from the authors or a person mandated by Linxo.
 *      If such an authorization is provided, any modified version
 *      or copy of the software has to contain this header.
 *
 * WARRANTIES:
 *      This software is made available by the authors in the hope
 *      that it will be useful, but without any warranty.
 *      Linxo is not liable for any consequence related to
 *      the use of the provided software.
 *
 * User: hugues
 * Date: 02/11/2015
 */

namespace LinxoClient\client\dto\tx;

require_once(__DIR__.'/../EntityInfo.php');
require_once(__DIR__.'/../../data/LinxoDate.php');
require_once(__DIR__.'/../../data/upcoming/IntervalUnit.php');
require_once(__DIR__.'/../../data/pfm/bank/TransactionType.php');

use LinxoClient\client\data\LinxoDate;
use LinxoClient\client\data\pfm\bank\TransactionType;
use LinxoClient\client\data\upcoming\IntervalUnit;
use LinxoClient\client\dto\EntityInfo;

class TransactionInfo extends EntityInfo
{

  /**
   * the date posted
   */
  var $date;

  /**
   * A User can change the month to which a transaction is assigned. She can change it to the month before the
   * {@link $date} or the month after. Depending on the choice, this date is the last day of the previous month
   * of the first day of the next month.
   *
   * When a user has not changed the value, or as selected the same month, it has the same value as {@link #date}.
   *
   * This is the date used when computing classification (budget) values
   */
  var $budgetDate;

  /**
   * the current label to display for this transaction
   */
  var $label;

  /**
   * the id the of currently assigned {@link CatInfo}
   */
  var $categoryId;

  /**
   * the amount of the transaction.
   */
  var $amount;

  /**
   * the check number when available, or null otherwise
   */
  var $checkNumber;

  /**
   * A User may find that this transaction is actually duplicated (bugs happen...). When she decides that the
   * When this value is {@code true}, this transactions is not used when computing budget values.
   *
   * {@code true} or {@code false} depending on the User's choice
   */
  var $duplicate;

  /**
   * the label as seen on the bank site when visited
   */
  var $originalLabel;

  /**
   * additional notes for this transaction
   */
  var $notes;

  /**
   * a list of {@link TagInfo} id.
   */
  var $tagIds;

  /**
   * the id of the bank account where this transactions is attached
   */
  var $bankAccountId;

  /// all attributes prefixed by "original" come from the Transaction as viewed at the bank

  /**
   * @type TransactionType
   * The type of this transaction. This cannot be changed.
   */
  var $originalType;

  /**
   * the date when the amount of this transactions actually impacts the balance of the account
   * (In France: "date de valeur"). {@code null} is returned when the values is not available.
   */
  var $originalDateAvailable;

  /**
   * the date when this transactions was initiated.
   * {@code null} is returned when the values is not available.
   */
  var $originalDateInitiated;

  /**
   * the postcode of the place where the transactions took place when available.
   * {@code null} is returned when the values is not available.
   */
  var $originalPostCode;

  /**
   * the city of the place where the transactions took place when available.
   * {@code null} is returned when the values is not available.
   */
  var $originalCity;

  /**
   * the country of the place where the transactions took place when available.
   * {@code null} is returned when the values is not available.
   */
  var $originalCountry;

  /**
   * the merchant name found when the label was processed when available. This name may be truncated
   * {@code null} is returned when the values is not available.
   * @see $originalThirdPartyLabelTruncated
   */
  var $originalThirdParty;

  /**
   * In some banks, the length of the {@link #originalLabel} is limited to a certain number of characters.
   * This may result in truncating the name of the merchant when the label is processed.
   *
   * {@code true} when the name of the merchant found when the label was processed when available was truncated.
   * {@code false} otherwise.
   * @see $originalThirdParty
   */
  var $originalThirdPartyLabelTruncated;

  /**
   * the account number to/from which this transactions is transferred from/to.
   * {@code null} is returned when the values is not available.
   */
  var $originalThirdPartyAccountNumber;

  var $originalCategory;

  /**
   * @type IntervalUnit
   * During the transaction processing, it may be found that this transaction is a recurring transaction. In this
   * case, the interval unit found is attached to the transaction.
   *
   * The potential interval unit of this transaction.
   * {@code null} is returned when the values is not available.
   */
  var $potentialIntervalUnit;

  /**
   * This value is used by credit cards.
   *
   * The day when the amount of this transaction impacts the account balance.
   * {@code null} is returned when the values is not available.
   */
  var $debitDate;

  /**
   * The reference. May be null.
   */
  var $reference;

  /**
   * TransactionInfo constructor.
   * @param \stdClass $that
   */
  public function __construct(\stdClass $that)
  {
    parent::__construct($that);

    if($that->debitDate != null){
      $this->debitDate = new LinxoDate($that->debitDate);
    }

    if($that->potentialIntervalUnit != null){
      $this->potentialIntervalUnit = IntervalUnit::valueOf($that->potentialIntervalUnit);
    }

    if($that->originalType != null){
      $this->originalType = TransactionType::valueOf($that->originalType);
    }

    $this->date = $that->date;
    $this->budgetDate = $that->budgetDate;
    $this->label = $that->label;
    $this->categoryId = $that->categoryId;
    $this->amount = $that->amount;
    $this->checkNumber = $that->checkNumber;
    $this->duplicate = $that->duplicate;
    $this->originalLabel = $that->originalLabel;
    $this->notes = $that->notes;
    $this->tagIds = $that->tagIds;
    $this->bankAccountId = $that->bankAccountId;
    $this->originalDateAvailable = $that->originalDateAvailable;
    $this->originalDateInitiated = $that->originalDateInitiated;
    $this->originalPostCode = $that->originalPostCode;
    $this->originalCity = $that->originalCity;
    $this->originalCountry = $that->originalCountry;
    $this->originalThirdParty = $that->originalThirdParty;
    $this->originalThirdPartyLabelTruncated = $that->originalThirdPartyLabelTruncated;
    $this->originalThirdPartyAccountNumber = $that->originalThirdPartyAccountNumber;
    $this->originalCategory = $that->originalCategory;
    $this->reference = $that->reference;

  }


}