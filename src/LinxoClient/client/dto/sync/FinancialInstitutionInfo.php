<?php
/**
 *  Copyright (c) 2015-2015 Linxo, All Rights Reserved.
 *
 * COPYRIGHT:
 *      This software is the property of Linxo.
 *      It cannot be copied, used, or modified without obtaining an
 *      authorization from the authors or a person mandated by Linxo.
 *      If such an authorization is provided, any modified version
 *      or copy of the software has to contain this header.
 *
 * WARRANTIES:
 *      This software is made available by the authors in the hope
 *      that it will be useful, but without any warranty.
 *      Linxo is not liable for any consequence related to
 *      the use of the provided software.
 *
 * User: hugues
 * Date: 27/10/2015
 */

namespace LinxoClient\client\dto\sync;

require_once(__DIR__.'/../EntityInfo.php');
require_once(__DIR__.'/Key.php');

use LinxoClient\client\dto\EntityInfo;

class FinancialInstitutionInfo extends EntityInfo
{
  /** @type string */
  var $name;
  /** @type string */
  var $branchName;
  /** @type string */
  var $logoUrl;
  /** @type string */
  var $bankUrl;

  /** @type bool */
  var $sync;
  /** @type bool */
  var $show;

  /** @type string */
  var $unAvailableMessage;

  /** @type string */
  var $supportUrl;

  /** @type string */
  var $supportText;

  /** @array (Key) */
  var $keys;

  /**
   * FinancialInstitution constructor.
   * @param \stdClass $that
   */
  public function __construct(\stdClass $that)
  {
    parent::__construct($that);

    $this->name = $that->name;
    $this->branchName = $that->branchName;
    $this->logoUrl = $that->logoUrl;
    $this->bankUrl = $that->bankUrl;

    $this->sync = $that->sync;
    $this->show = $that->show;

    $this->unAvailableMessage = $that->unAvailableMessage;
    $this->supportUrl = $that->supportUrl;
    $this->supportText = $that->supportText;

    if($that->keys != null) {
      $this->keys = array();
      foreach($that->keys as $stdKey) {
        $this->keys[] = new Key($stdKey);
      }
    }
  }


}