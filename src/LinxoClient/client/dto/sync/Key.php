<?php
/**
 *  Copyright (c) 2015-2015 Linxo, All Rights Reserved.
 *
 * COPYRIGHT:
 *      This software is the property of Linxo.
 *      It cannot be copied, used, or modified without obtaining an
 *      authorization from the authors or a person mandated by Linxo.
 *      If such an authorization is provided, any modified version
 *      or copy of the software has to contain this header.
 *
 * WARRANTIES:
 *      This software is made available by the authors in the hope
 *      that it will be useful, but without any warranty.
 *      Linxo is not liable for any consequence related to
 *      the use of the provided software.
 *
 * User: hugues
 * Date: 27/10/2015
 */

namespace LinxoClient\client\dto\sync;

require_once(__DIR__.'/../EntityInfo.php');
require_once(__DIR__.'/../../../infrastructure/BasicEnum.php');

use LinxoClient\client\dto\EntityInfo;
use LinxoClient\infrastructure\BasicEnum;

abstract class KeyType extends BasicEnum {
  const TextField        = 'TextField';     // The key should be entered through a simple text field
  const Keyboard         = 'Keyboard';      // The key should be entered through virtual keyboard
  const PasswordField    = 'PasswordField'; // The key should be entered through a password text field
  const HiddenField      = 'HiddenField';   // The key should be a hidden input field, not entered by the user (typically, the value of the label)
  const Date             = 'Date';          // The key should be entered through 3 text fields (dd MM yyyy)
  const OkCancel         = 'OkCancel';      // 2 buttons. Label field contains the text of the Ok button and optionnally the text of the Cancel button: Ok|Cancel
  const Info             = 'Info';          // Just text with elements that can be filled on the fly. Example label: 'Send SMS to {phone}'
  const Error            = 'Error';         // Just text with elements that can be filled on the fly. Example label: 'Send SMS to {phone}'
  const Timeout          = 'Timeout';       // Works as an Info field with a special 'timeout' argument (in seconds). The UI should substitute the {timeout} arg with a count down.
  const Oauth            = 'Oauth';         // The key should be entered through the bank API interface
}

class Key extends EntityInfo
{
  var $key;
  var $label;
  var $type;
  var $regexp;

  /**
   * Key constructor.
   */
  public function __construct(\stdClass $that)
  {
    parent::__construct($that);

    $this->key = $that->key;
    $this->label = $that->label;
    $this->regexp = $that->regexp;
    $this->type = KeyType::valueOf($that->type);
  }


}