<?php
/**
 *  Copyright (c) 2015-2015 Linxo, All Rights Reserved.
 *
 * COPYRIGHT:
 *      This software is the property of Linxo.
 *      It cannot be copied, used, or modified without obtaining an
 *      authorization from the authors or a person mandated by Linxo.
 *      If such an authorization is provided, any modified version
 *      or copy of the software has to contain this header.
 *
 * WARRANTIES:
 *      This software is made available by the authors in the hope
 *      that it will be useful, but without any warranty.
 *      Linxo is not liable for any consequence related to
 *      the use of the provided software.
 *
 * User: hugues
 * Date: 26/10/2015
 */

namespace LinxoClient\client\dto\user;



require_once ( __DIR__. "/../../data/permissions/Feature.php");
require_once ( __DIR__. "/../EntityInfo.php");
use LinxoClient\client\data\permissions\Feature;
use LinxoClient\client\dto\EntityInfo;


class PermissionInfo extends EntityInfo
{
  /**
   * @type Feature
   */
  var $feature;

  var $value;

  /**
   * PermissionInfo constructor.
   *
   */
  public function __construct(\stdClass $that)
  {
    parent::__construct($that);

    $this->value = $that->value;
    $this->feature = Feature::valueOf($that->feature);

  }


}