<?php
/**
 *  Copyright (c) 2015-2015 Linxo, All Rights Reserved.
 *
 * COPYRIGHT:
 *      This software is the property of Linxo.
 *      It cannot be copied, used, or modified without obtaining an
 *      authorization from the authors or a person mandated by Linxo.
 *      If such an authorization is provided, any modified version
 *      or copy of the software has to contain this header.
 *
 * WARRANTIES:
 *      This software is made available by the authors in the hope
 *      that it will be useful, but without any warranty.
 *      Linxo is not liable for any consequence related to
 *      the use of the provided software.
 *
 * User: hugues
 * Date: 26/10/2015
 */

namespace LinxoClient\client\dto\user;


require_once ( __DIR__. "/../EntityInfo.php");
use LinxoClient\client\dto\EntityInfo;

require_once ( __DIR__. "/../../data/LinxoDate.php");
use LinxoClient\client\data\LinxoDate;


require_once ( __DIR__. "/../../data/subscription/BillingCycle.php");
require_once ( __DIR__. "/../../data/subscription/DealSource.php");

class DealInfo extends EntityInfo
{
  /**
   * @type date
   */
  var $creationTime;

  /**
   * @type date
   */
  var $endTime;

  var $recurring;
  var $billingCycle;
  var $cyclePrice;
  var $commercialName;
  var $dealSource;

  /**
   * @type date
   */
  var $nextPaymentDate;
  /**
   * @type date
   */
  var $lastRenewalMailSentTime;

  /**
   * @type LinxoDate
   */
  var $renewalSuggestionTime;


}


