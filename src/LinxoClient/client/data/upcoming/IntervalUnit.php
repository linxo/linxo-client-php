<?php
/**
 *  Copyright (c) 2015-2015 Linxo, All Rights Reserved.
 *
 * COPYRIGHT:
 *      This software is the property of Linxo.
 *      It cannot be copied, used, or modified without obtaining an
 *      authorization from the authors or a person mandated by Linxo.
 *      If such an authorization is provided, any modified version
 *      or copy of the software has to contain this header.
 *
 * WARRANTIES:
 *      This software is made available by the authors in the hope
 *      that it will be useful, but without any warranty.
 *      Linxo is not liable for any consequence related to
 *      the use of the provided software.
 *
 * User: hugues
 * Date: 02/11/2015
 */

namespace LinxoClient\client\data\upcoming;

require_once(__DIR__."/../../../infrastructure/BasicEnum.php");

use LinxoClient\infrastructure\BasicEnum;

/**
 * Describes the unit used to compute time intervals.
 */
class IntervalUnit extends BasicEnum
{
  const once = 'once';
  const day = 'day';
  const week = 'week';
  const month = 'month';
  const year = 'year';
  const not_supported = 'not_supported';

}