<?php
/**
 *  Copyright (c) 2015-2015 Linxo, All Rights Reserved.
 *
 * COPYRIGHT:
 *      This software is the property of Linxo.
 *      It cannot be copied, used, or modified without obtaining an
 *      authorization from the authors or a person mandated by Linxo.
 *      If such an authorization is provided, any modified version
 *      or copy of the software has to contain this header.
 *
 * WARRANTIES:
 *      This software is made available by the authors in the hope
 *      that it will be useful, but without any warranty.
 *      Linxo is not liable for any consequence related to
 *      the use of the provided software.
 *
 * User: hugues
 * Date: 02/11/2015
 */

namespace LinxoClient\client\data\support;


class Version
{
  /**
   * version numbers are ordered into this array with
   * MAJOR at pos = 0
   * MINOR at pos = 1
   * MICRO at pos = 2
   * PATCH at pos = 3
   */
  var $version;

  /**
   * Version constructor.
   * @param $versionString
   */
  public function __construct(string $versionString)
  {
    $this->version = self::parseVersion($versionString);
  }

  /**
   * @param string $versionString
   * @return array
   */
  static function parseVersion($versionString)
  {
    $result = array();

    // The Pattern class is not available in GWT. So we need to parse this manually.
    // Below is the regexp we're trying to parse.
    //    Pattern p = Pattern.compile("(\\d+)\\.(\\d+)(?:\\.(\\d+)(?:-(\\d+))?)?(?:-SNAPSHOT)?");
    if(! preg_match("(\\d+)\\.(\\d+)(?:\\.(\\d+)(?:-(\\d+))?)?(?:-SNAPSHOT)?", $versionString)){
      throw new \InvalidArgumentException("Incompatible version number provided: [$versionString]");
    }

    $groups = preg_split("[\\.\\-]", $versionString);
    $min = min(count($groups), 4);
    for($i = 0; $i < $min; $i++) {
      $number = intval($groups[$i]);
      $result[$i] = $number;
    }

    return $result;

  }

}