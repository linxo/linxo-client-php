<?php
/**
 *  Copyright (c) 2015-2015 Linxo, All Rights Reserved.
 *
 * COPYRIGHT:
 *      This software is the property of Linxo.
 *      It cannot be copied, used, or modified without obtaining an
 *      authorization from the authors or a person mandated by Linxo.
 *      If such an authorization is provided, any modified version
 *      or copy of the software has to contain this header.
 *
 * WARRANTIES:
 *      This software is made available by the authors in the hope
 *      that it will be useful, but without any warranty.
 *      Linxo is not liable for any consequence related to
 *      the use of the provided software.
 *
 * User: hugues
 * Date: 02/11/2015
 */

namespace LinxoClient\client\data\pfm\bank;

require_once(__DIR__.'/../../../../infrastructure/BasicEnum.php');

use LinxoClient\infrastructure\BasicEnum;

class TransactionType extends BasicEnum
{
  /** generic credit. */
  const Credit = 'Credit';

  /** generic debit. */
  const Debit = 'Debit';

  /** interest paid or earned (depends on amount sign, paid: &lt;0, earned: &gt;0) */
  const Interest = 'Interest';

  /** dividend */
  const Dividend = 'Dividend';

  /** bank fee */
  const BankFee = 'BankFee';

  /** deposit. */
  const Deposit = 'Deposit';

  /**
   * Atm transaction debit or credit (depends on amount sign, debit &lt;0, credit &gt;0)
   * Can be a cash withdrawal, but in an Atm/DAB, not a branch
   * @see #Cash for cash withdrawal in a branch
   */
  const Atm = 'Atm';

  /**
   * point of sale,
   * credit card transactions
   */
  const PointOfSale = 'PointOfSale';

  /**
   * Payment of a credit card due amount, complete or partial
   */
  const CreditCardPayment = 'CreditCardPayment';

  /**
   * transfer identified within the provider
   * - 'virement recu/emis' locally in a provider group
   */
  const InternalTransfer = 'InternalTransfer';

  /**
   * potential transfer
   * - 'virement recu/emis' but not identified within this provider group
   */
  const PotentialTransfer = 'PotentialTransfer';

  /** check */
  const Check = 'Check';

  /**
   * Electronic payment
   * includes:
   * - TIP
   * - Telereglement
   * @see #DirectDebit for "prelevements"
   */
  const ElectronicPayment = 'ElectronicPayment';

  /** cash withdrawal in a branch */
  const Cash = 'Cash';

  /** direct deposit (for instance: salary) */
  const DirectDeposit = 'DirectDeposit';

  /** merchant-initiated debit
   * for instance:
   * - "prelevements"
   *@see #ElectronicPayment for TIP, telereglement
   */
  const DirectDebit = 'DirectDebit';

  /**
   * Repeating payment
   * for instance 'virement permanent'
   */
  const RepeatingPayment = 'RepeatingPayment';

  /**
   * other, unknown
   */
  const Other = 'Other';


}