<?php
/**
 *  Copyright (c) 2015-2015 Linxo, All Rights Reserved.
 *
 * COPYRIGHT:
 *      This software is the property of Linxo.
 *      It cannot be copied, used, or modified without obtaining an
 *      authorization from the authors or a person mandated by Linxo.
 *      If such an authorization is provided, any modified version
 *      or copy of the software has to contain this header.
 *
 * WARRANTIES:
 *      This software is made available by the authors in the hope
 *      that it will be useful, but without any warranty.
 *      Linxo is not liable for any consequence related to
 *      the use of the provided software.
 *
 * User: hugues
 * Date: 26/10/2015
 */

namespace LinxoClient\client\data\pfm\bank;

require_once(__DIR__.'/../../../../infrastructure/BasicEnum.php');
use LinxoClient\infrastructure\BasicEnum;

abstract class AccountSynchroStatus extends BasicEnum
{
  const None          = 'None';            // manual account
  const Never         = 'Never';           // never synchronized yet
  const Running       = 'Runing';          // running
  const Success       = 'Success';         // success
  const FailedSingle  = 'FailedSingle';    // failed but the whole synchro didn't fail, the user may decide to close it
  const Unavailable   = 'Unavailable';     // The account was not found on the financial institution website, the user may decide to close it
  const Failed        = 'Failed';          // the whole synchro for the group failed
  const Closed        = 'Closed';          // closed

}


