<?php
/*
   Copyright (c) 2015-2015 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
 */

namespace LinxoClient\client\data\pfm\group;

require_once(__DIR__.'/../../../../infrastructure/BasicEnum.php');
use LinxoClient\infrastructure\BasicEnum;

abstract class SynchroStatus extends BasicEnum
{
  const None                    = 'None';                   // manual account group
  const Never                   = 'Never';                  // never synchronized yet
  const Running                 = 'Running';                // running
  const PartialSuccess          = 'PartialSuccess';         // some of the accounts failed
  const Success                 = 'Success';                // success
  const Failed                  = 'Failed';                 // failed
  const AuthFailed              = 'AuthFailed';             // failed because of authentication
  const TooManyAttempts         = 'TooManyAttempts';        // failed because of authentication and max failed attempts reached
  const ServiceUnavailable      = 'ServiceuUnavailable';    // failed because the web site was not available
  const PasswordChangeRequired  = 'PasswordChangeRequired'; // failed because the web site required a password change
  const UserActionRequired      = 'UserActionRequired';     // failed because the user has to do something before to be able to access his accounts
  const ChallengeRequired       = 'ChallengeRequired';      // failed because a challenge/response is required and the user was not assisting the process
  const ChallengeTimedOut       = 'ChallengeTimedOut';      // failed because a challenge/response is required and the user did not answer in the alloted time
  const ChallengeFailed         = 'ChallengeFailed';        // failed because a challenge/response is required and the user responded incorrectly
  const ChallengeCancelled      = 'ChallengeCancelled';     // failed because a challenge/response is required and the user cancelled the process
  const NeedsSecret             = 'NeedsSecret';
  const Closed                  = 'Closed';                 // The user decides to manually close the account group
}


