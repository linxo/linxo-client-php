<?php
/**
 *  Copyright (c) 2015-2015 Linxo, All Rights Reserved.
 *
 * COPYRIGHT:
 *      This software is the property of Linxo.
 *      It cannot be copied, used, or modified without obtaining an
 *      authorization from the authors or a person mandated by Linxo.
 *      If such an authorization is provided, any modified version
 *      or copy of the software has to contain this header.
 *
 * WARRANTIES:
 *      This software is made available by the authors in the hope
 *      that it will be useful, but without any warranty.
 *      Linxo is not liable for any consequence related to
 *      the use of the provided software.
 *
 * User: hugues
 * Date: 02/11/2015
 */

namespace LinxoClient\client\data\pfm\alert;

require_once(__DIR__.'/../../../../infrastructure/BasicEnum.php');
use LinxoClient\infrastructure\BasicEnum;

class AlertType extends BasicEnum
{

  /**
   * News alert type. In the case, Alert#getMessage()
   * contains the information to display/send.
   */
  const News = 'News';

  /**
   * Generic alert type. In the case, Alert#getMessage()
   * contains the information to display/send.
   */
  const GenericAlert = 'GenericAlert';

  /**
   * Low balance alert type.
   * Alert#getMessage() contains a string
   * representation of the accountId.
   */
  //@see com.linxo.services.pfm.impl.alert.BankAccountAlertHandler
  const LowBalance = 'LowBalance';

  /**
   * High spending alert type.
   * Alert#getMessage() contains a String
   * representation of the transactionId.
   */
  // @see com.linxo.services.pfm.impl.alert.TransactionAlertHandler
  const HighSpending = 'HighSpending';

  /**
   * High deposit alert type.
   * Alert#getMessage() contains a String
   * representation of the transactionId.
   */
  // @see com.linxo.services.pfm.impl.alert.TransactionAlertHandler
  const HighDeposit = 'HighDeposit';

  /**
   * BankFee alert type.
   * Alert#getMessage() contains a String
   * representation of the transactionId.
   */
  // @see com.linxo.services.pfm.impl.alert.TransactionAlertHandler
  const BankFee = 'BankFee';

  /**
   * TooManyAttempts have been performed with bad identifiers
   * Connection is blocked until the user provides his
   * credentials again.
   */
  const TooManyAttempts = 'TooManyAttempts';

  /**
   * The web site requires that the user changes his password
   * Connection is blocked until the user provides his
   * credentials again.
   */
  const PasswordChangeRequired = 'PasswordChangeRequired';

  /**
   * The web site requires that the user does something: fill
   * a form, validate some information, etc. Connection is blocked
   * until the user does what he is asked to do.
   */
  const UserActionRequired = 'UserActionRequired';
}