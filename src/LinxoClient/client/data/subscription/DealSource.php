<?php
/**
 *  Copyright (c) 2015-2015 Linxo, All Rights Reserved.
 *
 * COPYRIGHT:
 *      This software is the property of Linxo.
 *      It cannot be copied, used, or modified without obtaining an
 *      authorization from the authors or a person mandated by Linxo.
 *      If such an authorization is provided, any modified version
 *      or copy of the software has to contain this header.
 *
 * WARRANTIES:
 *      This software is made available by the authors in the hope
 *      that it will be useful, but without any warranty.
 *      Linxo is not liable for any consequence related to
 *      the use of the provided software.
 *
 * User: hugues
 * Date: 26/10/2015
 */



namespace LinxoClient\client\data\subscription;

require_once(__DIR__.'/../../../infrastructure/BasicEnum.php');
use LinxoClient\infrastructure\BasicEnum;

abstract class DealSource extends BasicEnum
{
  const Paypal          = 'Paypal';           // Paypal Payments
  const PaypalPromos    = 'P aypalPromos';    // Paypal Promotions
  const ItunesIphone    = 'ItunesIphone';     // Apple InApp's
  const ItunesIpad      = 'ItunesIpad';       // Apple InApp's
  const GooglePlay      = 'GooglePlay';       // Google InApp's
  const Support         = 'Support';          // Added by support (payed by check, etc)
  const FreeSupport     = 'FreeSupport';      // Offered by Linxo
  const FreeSponsorship = 'FreeSponsorship';  // Offered by Linxo
  const AccountGroup    = 'AccountGroup';     // Not used in Linxo
  const WindowsPhone    = 'WindowsPhone';     // Microsoft InApp's
  const Windows8        = 'Windows8';         // Microsoft InApp's
  const Partnership     = 'Partnership';      // Offered because of a partnership
}
