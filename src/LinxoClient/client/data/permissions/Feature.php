<?php
/**
 *  Copyright (c) 2015-2015 Linxo, All Rights Reserved.
 *
 * COPYRIGHT:
 *      This software is the property of Linxo.
 *      It cannot be copied, used, or modified without obtaining an
 *      authorization from the authors or a person mandated by Linxo.
 *      If such an authorization is provided, any modified version
 *      or copy of the software has to contain this header.
 *
 * WARRANTIES:
 *      This software is made available by the authors in the hope
 *      that it will be useful, but without any warranty.
 *      Linxo is not liable for any consequence related to
 *      the use of the provided software.
 *
 * User: hugues
 * Date: 26/10/2015
 */

namespace LinxoClient\client\data\permissions;

require_once(__DIR__.'/../../../infrastructure/BasicEnum.php');
use LinxoClient\infrastructure\BasicEnum;

abstract class Feature extends BasicEnum
{
  const VisibleHistory       = 'VisibleHistory';
  const ApiUtilization       = 'ApiUtilization';
  const PrioritizedSupport   = 'PrioritizedSupport';
  const BlockAdvertisement   = 'BlockAdvertisement';
  const EnhancedLogo         = 'EnhancedLogo';
  const ExtendedExports      = 'ExtendedExports';
  const CustomCategories     = 'CustomCategories';
  const UpcomingTransactions = 'UpcomingTransactions';

}