<?php
/*
   Copyright (c) 2015-2015 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
 */
namespace LinxoClient\client\json;

const CLIENT_BASE_PACKAGE = "LinxoClient.client.actions.";
const SERVER_BASE_PACKAGE = "com.linxo.gwt.rpc.client.";

const ERROR_RESULT_CLIENT_CLASS_NAME = '\LinxoClient\client\actions\ErrorResult';
const ERROR_RESULT_SERVER_CLASS_NAME = "com.linxo.gwt.server.support.json.ErrorResult";

class LinxoJsonSerializer
{

  public static function getJsonNameFromType($obj)
  {
    $className = get_class($obj);
    $className = str_replace("\\", ".", $className);
    $className = str_replace(CLIENT_BASE_PACKAGE, SERVER_BASE_PACKAGE, $className);

    return $className;
  }

  /**
   * @param $jsonName
   * @return string that represents the type from the
   * @throws \Exception
   */
  public static function getTypeFromJsonName($jsonName)
  {
    // First we take care of the well known
    if (strcmp(ERROR_RESULT_SERVER_CLASS_NAME , trim($jsonName)) == 0)
    {
      return ERROR_RESULT_CLIENT_CLASS_NAME;
    }

    // Then we do automatic package conversion
    $pos = strpos($jsonName, SERVER_BASE_PACKAGE);
    if($pos === FALSE || $pos !== 0)
    {
      throw new \Exception("Can not determine type for jsonName " . $jsonName, 0);
    }
    $shortNameWithDots = substr($jsonName, strlen(SERVER_BASE_PACKAGE));

    return "\\".str_replace(".", "\\", (CLIENT_BASE_PACKAGE . $shortNameWithDots));

  }
}