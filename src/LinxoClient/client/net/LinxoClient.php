<?php
/*
   Copyright (c) 2015-2015 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
 */
namespace LinxoClient\client\net;

require_once(__DIR__.'/../json/LinxoJsonSerializer.php');
use LinxoClient\client\json\LinxoJsonSerializer;

require_once(__DIR__.'/../../infrastructure/exceptions/TechnicalException.php');
use LinxoClient\infrastructure\exceptions;

require_once(__DIR__.'/../actions/LinxoAction.php');
require_once(__DIR__.'/../actions/LinxoRequest.php');
require_once(__DIR__.'/../actions/LinxoResponse.php');
require_once(__DIR__.'/../actions/LinxoResult.php');
use LinxoClient\client\actions;


const WRAPPER_VERSION = '0.1';

const PROTOCOL = "https://";
const PATH_AUTH = "/auth.page";
const PATH_JSON = "/json";

const PATH_TMP_DIR = "/tmp";

const API_VERSION = "1.6.6";

const USER_AGENT = 'LinxoClient/@VERSION@ (Api Version ';//.API_VERSION.')';


class LinxoClient
{

  // Server domain
  var $domain;

  var $jsonUrl;
  var $authUrl;

  // API key and secret
  var $apiKey;
  var $apiSecret;

  // Time offset with the server
  var $offset;

  // Cookie Store file
  var $cookieStore;

  var $jSessionId;
  var $linxoSession;

  //o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-
  //
  //      CONSTRUCTORS
  //
  //o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-


  public function __construct($domain, $apiKey, $secretKey)
  {
    $this->apiKey = $apiKey;
    $this->apiSecret = $secretKey;
    $this->domain = $domain;

    $this->authUrl = PROTOCOL . $this->domain . PATH_AUTH;
    $this->jsonUrl = PROTOCOL . $this->domain . PATH_JSON;

    $this->offset = 0;
  }

  //o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-
  //
  //      PUBLIC API
  //
  //o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-


  public function initialize()
  {
    if($this->cookieStore != null)
    {
      // already initialized
      return;
    }

    $this->cookieStore = tempnam (PATH_TMP_DIR, 'cookielinxo');

    // Initialize the session
    $ch = $this->newCurlHandle($this->authUrl, true);
    // As we initialize, we make sure we start a new cookie session.
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);

    $response = curl_exec($ch);
    if (curl_error($ch)) {
      throw new exceptions\TechnicalException(curl_error($ch), curl_errno($ch), null);
    }

    $this->updateSessionInfo($response, $ch);

    // todo $offset : Update $offset according to the server time

    curl_close($ch);

  }


  /**
   * Sends the given action to the server and returns the result,
   * or a LinxoError
   *
   * @param actions\LinxoAction $action
   * @return mixed extends \LinxoClient\client\actions\Linxo
   */
  public function sendAction(\LinxoClient\client\actions\LinxoAction $action)
  {
    $linxoRequest = $this->createRequest($action);

    $dataString = json_encode($linxoRequest);

    $ch = $this->newCurlHandle($this->jsonUrl);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);

    $response = curl_exec($ch);

    $this->updateSessionInfo($response, $ch);

    $body = substr($response, curl_getinfo($ch, CURLINFO_HEADER_SIZE));

    curl_close($ch);

    $body = str_replace(")]}'\n", '', $body);

    $responseObject = json_decode($body);

    $linxoResponse = new actions\LinxoResponse($responseObject);

    // The name of the result is contained in the resultName;
    $typeName = LinxoJsonSerializer::getTypeFromJsonName($linxoResponse->resultName);
    if(! class_exists($typeName)) {
      // If the class does not exist for real, then the error will trigger when trying
      // to load the file.
      /** @noinspection PhpIncludeInspection */
      require_once(__DIR__ . "/../../.." . str_replace("\\", "/", $typeName) . ".php");
    }
    return new $typeName($linxoResponse->result);
  }



  //o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-
  //
  //      PRIVATE API
  //
  //o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-


  private function createRequest($action)
  {
    $nonce = sha1(rand());
    $timeStamp = time();
    // todo $offset : Update $timeStamp according to the value of $offset

    // Create the Hash out of the variables above and the key and secret
    $hash = new actions\Hash(
        $nonce,
        $timeStamp,
        $this->apiKey,
        base64_encode(sha1($nonce.$timeStamp.$this->apiSecret))
    );

    // set the secret
    $action->secret = $this->linxoSession;

    // create the request object
    $linxoRequest = new actions\LinxoRequest(
        $action,
        LinxoJsonSerializer::getJsonNameFromType($action),
        $hash);

    return $linxoRequest;
  }

  /**
   * @param $endPoint
   * @param $startSession
   * @return resource a cURL handle on success, false on errors.
   */
  private function newCurlHandle($endPoint, $startSession = false, $enableSSLVerification = true)
  {
    $ch = curl_init($endPoint);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_COOKIESESSION, !$startSession);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_USERAGENT, USER_AGENT);

    // This may help in case the connection do not work well.
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $enableSSLVerification);

    $headers = array(
        'Content-Type: application/json',
        'X-LINXO-API-Version : ' . API_VERSION,
        'Accept-Charset : utf-8',
        'Accept: application/json',
        'Content-Type: application/json; charset=UTF-8',
    );

    if(! $startSession) {
      $headers[] = 'Cookie: LinxoSession=' . $this->linxoSession . ';JSESSIONID=' . $this->jSessionId . ';';
    }

    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    // maintain the session with cookies
    curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookieStore);
    curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookieStore);
    return $ch;
  }

  /**
   * Updates the LinxoSession and jSessionId according to the cookie headers returned in the response.
   */
  private function updateSessionInfo($response, $ch)
  {
    // We're just interested in the headers.
    $headers = substr($response, 0, curl_getinfo($ch, CURLINFO_HEADER_SIZE));
    // Parse the headers to extract the LinxoSession and the jSessionId;
    foreach (explode("\n", $headers) as $header) {
      $headerInfos = explode(':', $header);
      if ($headerInfos[0] == 'Set-Cookie') {
        $cookieInfos = explode(';', $headerInfos[1]);
        $sessionInfos = explode('=', $cookieInfos[0]);

        if (trim($sessionInfos[0]) == 'JSESSIONID')
          $this->jSessionId = trim($sessionInfos[1]);

        if (trim($sessionInfos[0]) == 'LinxoSession')
          $this->linxoSession = trim($sessionInfos[1]);
      }
    }
  }

}
?>