<?php
/**
 *  Copyright (c) 2015-2015 Linxo, All Rights Reserved.
 *
 * COPYRIGHT:
 *      This software is the property of Linxo.
 *      It cannot be copied, used, or modified without obtaining an
 *      authorization from the authors or a person mandated by Linxo.
 *      If such an authorization is provided, any modified version
 *      or copy of the software has to contain this header.
 *
 * WARRANTIES:
 *      This software is made available by the authors in the hope
 *      that it will be useful, but without any warranty.
 *      Linxo is not liable for any consequence related to
 *      the use of the provided software.
 *
 * User: hugues
 * Date: 02/11/2015
 */

namespace LinxoClient\client\actions\pfm;


require_once(__DIR__.'/../LinxoResult.php');
require_once(__DIR__.'/../../dto/tx/TransactionInfo.php');

use LinxoClient\client\actions\LinxoResult;
use LinxoClient\client\dto\tx\TransactionInfo;


/**
 * This bean returns the result for the "get transactions":
 * - the transactions, including duplicates,
 * - the count of transactions visible and available, including the duplicate transactions,
 * - the debit and credit amounts, excluding the duplicate transactions.
 */
class GetTransactionsResult extends LinxoResult
{
  /** array(TransactionInfo) */
  var $transactions;

  /** @var int number of transactions stored for the current search */
  var $availableCount;

  /** @var int number of transactions available for the current search.
   * This number may be smaller than $availableCount due to the user's current
   * permissions.
   */
  var $visibleCount;

  // TOTAL debits and credits for the current search
  var $credits;
  var $debits;

  /**
   * GetTransactionsResult constructor.
   * @param \stdClass $that
   */
  public function __construct(\stdClass $that)
  {
    parent::__construct($that);

    $this->availableCount = $that->availableCount;
    $this->visibleCount = $that->visibleCount;

    $this->debits   = $that->debits;
    $this->credits  = $that->credits;

    $this->transactions = array();
    if($that->transactions != null) {
      foreach($that->transactions as $stdTransaction) {
        $this->transactions[] = new TransactionInfo($stdTransaction);
      }
    }
  }


}