<?php
/**
 *  Copyright (c) 2015-2015 Linxo, All Rights Reserved.
 *
 * COPYRIGHT:
 *      This software is the property of Linxo.
 *      It cannot be copied, used, or modified without obtaining an
 *      authorization from the authors or a person mandated by Linxo.
 *      If such an authorization is provided, any modified version
 *      or copy of the software has to contain this header.
 *
 * WARRANTIES:
 *      This software is made available by the authors in the hope
 *      that it will be useful, but without any warranty.
 *      Linxo is not liable for any consequence related to
 *      the use of the provided software.
 *
 * User: hugues
 * Date: 28/10/2015
 */

namespace LinxoClient\client\actions\pfm\sync;

require_once(__DIR__.'/../../LinxoAction.php');

use LinxoClient\client\actions\LinxoAction;

/**
 * This action should be used for initial ListAccounts.
 *
 * Its parameters are :
 *  - a financial institution id: id of the bank we want to list the accounts for,
 *  - a list of Credential: the credentials the user entered. Any credential whose
 *    the key contains "secret" should already be encrypted in this action.
 *  - a boolean indicating whether or not the group we want to create has to be
 *    "semi-auto" (i.e. Linxo doesn't remember all the credentials). This parameter
 *    may be null (not appear in the json at all).
 *
 * When the server receives this action, a "ListAccounts" job starts. Clients
 * should listen to AccountListUpdateEvent events to get the updates for this
 * job. The final update will contain the list of accounts found on the bank
 * web site, or an error status in case of problem.
 */
class ListAccountsInFinancialInstitutionAction extends LinxoAction
{
  var $financialInstitutionId;
  var $credentials;
  var $semiAuto;

  /**
   * ListAccountsInFinancialInstitutionAction constructor.
   * @param $financialInstitutionId
   * @param $credentials
   * @param $semiAuto
   */
  public function __construct($financialInstitutionId, $credentials, $semiAuto = false)
  {
    $this->financialInstitutionId = $financialInstitutionId;
    $this->credentials = $credentials;
    $this->semiAuto = $semiAuto;
  }



}