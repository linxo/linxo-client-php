<?php
/**
 *  Copyright (c) 2015-2015 Linxo, All Rights Reserved.
 *
 * COPYRIGHT:
 *      This software is the property of Linxo.
 *      It cannot be copied, used, or modified without obtaining an
 *      authorization from the authors or a person mandated by Linxo.
 *      If such an authorization is provided, any modified version
 *      or copy of the software has to contain this header.
 *
 * WARRANTIES:
 *      This software is made available by the authors in the hope
 *      that it will be useful, but without any warranty.
 *      Linxo is not liable for any consequence related to
 *      the use of the provided software.
 *
 * User: hugues
 * Date: 28/10/2015
 */

namespace LinxoClient\client\actions\pfm\sync;

require_once(__DIR__.'/../../../dto/EntityInfo.php');

use LinxoClient\client\dto\EntityInfo;

class Credential extends EntityInfo
{
  var $key;
  var $value;

  /**
   * Credential constructor.
   * @param $key
   */
  public function __construct($id, $key, $value)
  {
    // send an empty \stdClass to the parent class contructor
    parent::__construct(new \stdClass);
    $this->id = $id;
    $this->key = $key;
    $this->value = $value;
  }


}