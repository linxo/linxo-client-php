<?php
/**
 *  Copyright (c) 2015-2015 Linxo, All Rights Reserved.
 *
 * COPYRIGHT:
 *      This software is the property of Linxo.
 *      It cannot be copied, used, or modified without obtaining an
 *      authorization from the authors or a person mandated by Linxo.
 *      If such an authorization is provided, any modified version
 *      or copy of the software has to contain this header.
 *
 * WARRANTIES:
 *      This software is made available by the authors in the hope
 *      that it will be useful, but without any warranty.
 *      Linxo is not liable for any consequence related to
 *      the use of the provided software.
 *
 * User: hugues
 * Date: 27/10/2015
 */

namespace LinxoClient\client\actions\pfm\sync;

require_once(__DIR__.'/../../LinxoAction.php');

use LinxoClient\client\actions\LinxoAction;

/**
 * This action returns the credential keys for the financial institution.
 * It also returns the pgp rsa public key that must be used to encrypt the
 * credentials that contain the string 'secret'.
 *
 * This action must be performed with a logged in user, as the pgp rsa key
 * pair is stored in the session. The key can only be used once, for the given
 * financial institution id.
 */
class GetFinancialInstitutionAction extends LinxoAction
{
  var $financialInstitutionId;

  /**
   * GetFinancialInstitutionAction constructor.
   * @param $financialInstitutionId
   */
  public function __construct($financialInstitutionId)
  {
    $this->financialInstitutionId = $financialInstitutionId;
  }

}