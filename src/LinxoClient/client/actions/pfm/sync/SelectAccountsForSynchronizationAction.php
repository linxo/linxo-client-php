<?php
/**
 *  Copyright (c) 2015-2015 Linxo, All Rights Reserved.
 *
 * COPYRIGHT:
 *      This software is the property of Linxo.
 *      It cannot be copied, used, or modified without obtaining an
 *      authorization from the authors or a person mandated by Linxo.
 *      If such an authorization is provided, any modified version
 *      or copy of the software has to contain this header.
 *
 * WARRANTIES:
 *      This software is made available by the authors in the hope
 *      that it will be useful, but without any warranty.
 *      Linxo is not liable for any consequence related to
 *      the use of the provided software.
 *
 * User: hugues
 * Date: 28/10/2015
 */

namespace LinxoClient\client\actions\pfm\sync;

require_once(__DIR__.'/../../LinxoAction.php');

use LinxoClient\client\actions\LinxoAction;


/**
 * This action should be used to add the accounts the client selected for synchronization
 * after it got the account list from the bank (at the end of the initial list accounts job).
 *
 * Use UpdateAccountListAction to add/remove/close/mark as FailedSingle accounts after
 * getting the account list from the bank for an existing group (list accounts job).
 */
class SelectAccountsForSynchronizationAction extends LinxoAction
{
  var $accountsSelected;

  /**
   * SelectAccountsForSynchronizationAction constructor.
   * @param $accountsSelected - array of {@link LinxoClient\client\dto\account\ProviderAccountInfo}.
   */
  public function __construct($accountsSelected)
  {
    $this->accountsSelected = $accountsSelected;
  }


}