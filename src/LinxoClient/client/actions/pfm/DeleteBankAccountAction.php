<?php
/**
 *  Copyright (c) 2015-2015 Linxo, All Rights Reserved.
 *
 * COPYRIGHT:
 *      This software is the property of Linxo.
 *      It cannot be copied, used, or modified without obtaining an
 *      authorization from the authors or a person mandated by Linxo.
 *      If such an authorization is provided, any modified version
 *      or copy of the software has to contain this header.
 *
 * WARRANTIES:
 *      This software is made available by the authors in the hope
 *      that it will be useful, but without any warranty.
 *      Linxo is not liable for any consequence related to
 *      the use of the provided software.
 *
 * User: hugues
 * Date: 02/11/2015
 */

namespace LinxoClient\client\actions\pfm;

require_once(__DIR__.'/../LinxoAction.php');

use LinxoClient\client\actions\LinxoAction;

class DeleteBankAccountAction extends LinxoAction
{
  var $accountGroupId;
  var $bankAccountId;

  /**
   * DeleteBankAccountAction constructor.
   * @param $accountGroupId
   * @param $bankAccountId
   */
  public function __construct($accountGroupId, $bankAccountId)
  {
    $this->accountGroupId = $accountGroupId;
    $this->bankAccountId = $bankAccountId;
  }


}