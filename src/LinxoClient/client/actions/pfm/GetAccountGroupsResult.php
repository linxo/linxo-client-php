<?php
/*
   Copyright (c) 2015-2015 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
 */
namespace LinxoClient\client\actions\pfm;

require_once(__DIR__.'/../LinxoResult.php');
require_once(__DIR__.'/../../dto/account/AccountGroupInfo.php');

use LinxoClient\client\actions\LinxoResult;
use LinxoClient\client\dto\account\AccountGroupInfo;

class GetAccountGroupsResult extends LinxoResult
{
  /**
   * @type array<AccountGroupInfo>
   */
  var $accountGroups;

  /**
   * GetAccountGroupsResult constructor.
   */
  public function __construct(\stdClass $that)
  {
    parent::__construct($that);

    $this->accountGroups = array();

    if($that->accountGroups != null) {
      foreach($that->accountGroups as $stdAccountGroup)
      {
        $this->accountGroups[] = new AccountGroupInfo($stdAccountGroup);
      }
    }
  }


}