<?php
/**
 *  Copyright (c) 2015-2015 Linxo, All Rights Reserved.
 *
 * COPYRIGHT:
 *      This software is the property of Linxo.
 *      It cannot be copied, used, or modified without obtaining an
 *      authorization from the authors or a person mandated by Linxo.
 *      If such an authorization is provided, any modified version
 *      or copy of the software has to contain this header.
 *
 * WARRANTIES:
 *      This software is made available by the authors in the hope
 *      that it will be useful, but without any warranty.
 *      Linxo is not liable for any consequence related to
 *      the use of the provided software.
 *
 * User: hugues
 * Date: 30/10/2015
 */

namespace LinxoClient\client\actions\pfm;

require_once(__DIR__.'/../LinxoAction.php');
require_once(__DIR__.'/../../data/CategoryType.php');
require_once(__DIR__.'/../../data/pfm/bank/AccountType.php');

use LinxoClient\client\actions\LinxoAction;
use LinxoClient\client\data\CategoryType;
use LinxoClient\client\data\pfm\bank\AccountType;

class GetTransactionsAction extends LinxoAction
{

  /** @type AccountType */
  var $accountType;

  var $accountId;
  var $accountReference;

  /** @type array (string) */
  var $labels;

  var $categoryId;
  var $tagId;

  var $startDate;
  var $endDate;
  var $debitDate;

  var $minAmount;
  var $maxAmount;

  var $useBudgetDate;
  var $useTrendsAccount;

  var $loadSubCategories;
  var $loadOnlyTaggedTransactions;

  /** @type CategoryType */
  var $categoryType;

  var $extCustomType;

  // PAGING

  var $startRow;
  var $numRows;


  /**
   * GetTransactionsAction constructor.
   * @param AccountType $accountType - filters transactions that are on the given accountType
   * @param $accountId - filters transactions that are on the account represented by this id
   * @param $accountReference - filters transactions that are on the account represented by this reference. Can be used instead of the account id.
   * @param array $labels - filter on the transactions that match the given tokens. The token may be on the label, note of check number of the transaction.
   * @param $categoryId - filters transactions that are assigned the category with the given id
   * @param $tagId - filters transactions that are assigned the tag with the given id
   * @param $startDate - date format "M/d/yy", inclusive
   * @param $endDate - date format "M/d/yy", inclusive
   * @param $debitDate - date format "M/d/yy", exact match
   * @param $minAmount - min amount, inclusive
   * @param $maxAmount - max amount, inclusive
   * @param $useBudgetDate - indicates if the date filters should be taken on the {@link com.linxo.client.dto.tx.TransactionInfo#budgetDate budget date}rather that the {@link com.linxo.client.dto.tx.TransactionInfo#date}
   * @param $useTrendsAccount - filters transactions that are on an account selected for the Trends (Graphics)
   * @param $loadSubCategories - indicates if we should also load transactions attached to the sub-categories when filtering on categories
   * @param $loadOnlyTaggedTransactions - when true, we load only transactions that are assigned at least one tag.
   * @param CategoryType $categoryType - filters transactions that are assigned a category with the given categoryType
   * @param $extCustomType - filter transactions that have this extCustomType
   * @param $startRow - The row number of the first transaction to load (paging), required
   * @param $numRows - the max number of rows to return (paging), required
   */
  public function __construct(
      $startRow,
      $numRows,
      $accountType = null,
      $accountId = null,
      $accountReference = null,
      array $labels = null,
      $categoryId = null,
      $tagId = null,
      $startDate = null,
      $endDate = null,
      $debitDate = null,
      $minAmount = null,
      $maxAmount = null,
      $useBudgetDate = null,
      $useTrendsAccount = null,
      $loadSubCategories = null,
      $loadOnlyTaggedTransactions = null,
      $categoryType = null,
      $extCustomType = null)
  {
    $this->accountType = $accountType;
    $this->accountId = $accountId;
    $this->accountReference = $accountReference;
    $this->labels = $labels;
    $this->categoryId = $categoryId;
    $this->tagId = $tagId;
    $this->startDate = $startDate;
    $this->endDate = $endDate;
    $this->debitDate = $debitDate;
    $this->minAmount = $minAmount;
    $this->maxAmount = $maxAmount;
    $this->useBudgetDate = $useBudgetDate;
    $this->useTrendsAccount = $useTrendsAccount;
    $this->loadSubCategories = $loadSubCategories;
    $this->loadOnlyTaggedTransactions = $loadOnlyTaggedTransactions;
    $this->categoryType = $categoryType;
    $this->extCustomType = $extCustomType;
    $this->startRow = $startRow;
    $this->numRows = $numRows;
  }


}