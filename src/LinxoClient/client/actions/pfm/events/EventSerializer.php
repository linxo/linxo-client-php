<?php
/**
 *  Copyright (c) 2015-2015 Linxo, All Rights Reserved.
 *
 * COPYRIGHT:
 *      This software is the property of Linxo.
 *      It cannot be copied, used, or modified without obtaining an
 *      authorization from the authors or a person mandated by Linxo.
 *      If such an authorization is provided, any modified version
 *      or copy of the software has to contain this header.
 *
 * WARRANTIES:
 *      This software is made available by the authors in the hope
 *      that it will be useful, but without any warranty.
 *      Linxo is not liable for any consequence related to
 *      the use of the provided software.
 *
 * User: hugues
 * Date: 28/10/2015
 */

namespace LinxoClient\client\actions\pfm\events;

require_once(__DIR__.'/AccountListUpdateEvent.php');
require_once(__DIR__.'/SynchronizationUpdateEvent.php');
require_once(__DIR__.'/../../../../infrastructure/exceptions/TechnicalException.php');

use LinxoClient\clients\action\pfm\events\AccountListUpdateEvent;
use LinxoClient\clients\action\pfm\events\SynchronizationUpdateEvent;
use LinxoClient\infrastructure\exceptions\TechnicalException;



class EventSerializer
{
  public static function deserialize(\stdClass $stdEvent)
  {
    // A 'type' element is added to the JSON by the server to help determine
    // the event type. This type should be removed from the stdClass.
    if( strcasecmp($stdEvent -> type, 'SynchronizationUpdateEvent') == 0 )
    {
      return new SynchronizationUpdateEvent($stdEvent);
    }
    elseif ( strcasecmp($stdEvent -> type, 'AccountListUpdateEvent') == 0)
    {
      return new AccountListUpdateEvent($stdEvent);
    }

    // TODO :dealsUpdateEvent
    // TODO :permissionsUpdateEvent
    // TODO :oauthTokenEvent
    // TODO :userInfoUpdatedEvent

    throw new TechnicalException("Unsupported event type [".$stdEvent -> type."] (may not be implemented yet)");
  }

}