<?php
/**
 *  Copyright (c) 2015-2015 Linxo, All Rights Reserved.
 *
 * COPYRIGHT:
 *      This software is the property of Linxo.
 *      It cannot be copied, used, or modified without obtaining an
 *      authorization from the authors or a person mandated by Linxo.
 *      If such an authorization is provided, any modified version
 *      or copy of the software has to contain this header.
 *
 * WARRANTIES:
 *      This software is made available by the authors in the hope
 *      that it will be useful, but without any warranty.
 *      Linxo is not liable for any consequence related to
 *      the use of the provided software.
 *
 * User: hugues
 * Date: 26/10/2015
 */


namespace LinxoClient\clients\action\pfm\events;

require_once(__DIR__.'/../../../../infrastructure/BasicEnum.php');
use LinxoClient\clients\action\pfm\events\AbstractSyncEvent;
use LinxoClient\infrastructure\BasicEnum;

require_once(__DIR__.'/../../../dto/account/OperationUpdateInfo.php');
use LinxoClient\client\dto\account\OperationUpdateInfo;

require_once(__DIR__.'/../../../dto/account/ProviderAccountInfo.php');
use LinxoClient\client\dto\account\ProviderAccountInfo;

require_once(__DIR__.'/AbstractSyncEvent.php');

abstract class AccountListUpdateEventUpdateType extends BasicEnum
{
  const Update = 'Update';
  const Success = 'Success';
  const Failure = 'Failuire';
}

abstract class AccountListUpdateEventErrorType extends BasicEnum
{
  const Error = 'Error';
  const AuthError = 'AuthError';
  const InvalidCredentials = 'InvalidCredentials';
  const TooManyAttempts = 'TooManyAttempts';
  const ServiceUnavailable = 'ServiceUnavailable';
  const PasswordChangeRequired = 'PasswordChangeRequired';
  const UserActionRequired = 'UserActionRequired';
  const ChallengeRequired = 'ChallengeRequired';      // failed because a challenge/response is required and the user was not assisting the process
  const ChallengeTimedOut = 'ChallengeTimedOut';      // failed because a challenge/response is required and the user did not answer in the alloted time
  const ChallengeFailed   = 'ChallengeFailed';        // failed because a challenge/response is required and the user responded incorrectly
  const ChallengeCancelled = 'ChallengeCancelled';    // failed because a challenge/response is required and the user cancelled the process
}

class AccountListUpdateEvent extends AbstractSyncEvent
{
  var $accountGroupId;

  /**
   * @type AccountListUpdateEventUpdateType
   */
  var $updateType;

  /**
   * @type AccountListUpdateEventErrorType
   */
  var $errorType;

  /**
   * @type OperationUpdateInfo
   */
  var $operationUpdate;

  /**
   * @array(ProviderAccountInfo)
   */
  var $providerAccounts;

  /**
   * AccountListUpdateEvent constructor.
   */
  public function __construct(\stdClass $that)
  {
    parent::__construct($that);

    $this->accountGroupId = $that->accountGroupId;

    if ($that->updateType != null) {
      $this->updateType = AccountListUpdateEventUpdateType::valueOf($that->updateType);
    }
    if ($that->errorType != null) {
      $this->errorType = AccountListUpdateEventErrorType::valueOf($that->errorType);
    }
    if ($that->operationUpdate != null) {
      $this->operationUpdate = new OperationUpdateInfo($that->operationUpdate);
    }
    $this->providerAccounts = array();
    foreach( $that->providerAccounts as $stdProviderAccount ) {
      $this->providerAccounts[] = new ProviderAccountInfo($stdProviderAccount);
    }
  }
}

