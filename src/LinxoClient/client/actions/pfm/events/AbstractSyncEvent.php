<?php
/**
 *  Copyright (c) 2015-2015 Linxo, All Rights Reserved.
 *
 * COPYRIGHT:
 *      This software is the property of Linxo.
 *      It cannot be copied, used, or modified without obtaining an
 *      authorization from the authors or a person mandated by Linxo.
 *      If such an authorization is provided, any modified version
 *      or copy of the software has to contain this header.
 *
 * WARRANTIES:
 *      This software is made available by the authors in the hope
 *      that it will be useful, but without any warranty.
 *      Linxo is not liable for any consequence related to
 *      the use of the provided software.
 *
 * User: hugues
 * Date: 26/10/2015
 */

namespace LinxoClient\clients\action\pfm\events;

require_once(__DIR__.'/SyncEvent.php');

abstract class AbstractSyncEvent implements SyncEvent
{
  var $eventNumber;

  /**
   * AbstractSyncEvent constructor.
   */
  public function __construct(\stdClass $that)
  {
    $this->eventNumber = $that->eventNumber;
  }


  public function setEventNumber($eventNumber)
  {
    $this->eventNumber = $eventNumber;
  }

  public function getEventNumber()
  {
    return $this->eventNumber;
  }

}