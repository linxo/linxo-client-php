<?php
/*
   Copyright (c) 2015-2015 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
 */

namespace LinxoClient\client\actions\auth;

require_once(__DIR__.'/../LinxoResult.php');
use LinxoClient\client\actions\LinxoResult;

class LoginResult extends LinxoResult
{
  var $userId;
  var $firstName;
  var $lastName;
  var $isActivated;
  var $askForTermsAndConditions;

  // if things go bad
  var $blocked;

  /**
   * LoginResult constructor.
   * @param \stdClass $that
   */
  public function __construct(\stdClass $that)
  {
    parent::__construct($that);

    $this->userId = $that->userId;
    $this->firstName = $that->firsName;
    $this->lastName = $that->lastName;
    $this->isActivated = $that->isActivated;
    $this->askForTermsAndConditions = $that->askForTermsAndConditions;
    $this->blocked = $that->blocked;
  }


}