<?php
/*
   Copyright (c) 2015-2015 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
 */

namespace LinxoClient\client\actions\auth;

require_once(__DIR__.'/../LinxoAction.php');
use LinxoClient\client\actions\LinxoAction;

/**
 * This action allows to login a given user to Linxo.
 * Note:
 *   This action can only be user with un-limited api-key. Using this action with a limited key
 *   will result in an error.
 */
class LoginAction extends LinxoAction
{
  var $email;
  var $password;
  var $rememberMe;
  var $secretMode;


  public function __construct($email, $password, $rememberMe= FALSE, $secretMode = FALSE)
  {
    $this->email  = $email;
    $this->password = $password;
    $this->rememberMe = $rememberMe;
    $this->secretMode = $secretMode;
  }


}
