<?php
/**
 *  Copyright (c) 2015-2015 Linxo, All Rights Reserved.
 *
 * COPYRIGHT:
 *      This software is the property of Linxo.
 *      It cannot be copied, used, or modified without obtaining an
 *      authorization from the authors or a person mandated by Linxo.
 *      If such an authorization is provided, any modified version
 *      or copy of the software has to contain this header.
 *
 * WARRANTIES:
 *      This software is made available by the authors in the hope
 *      that it will be useful, but without any warranty.
 *      Linxo is not liable for any consequence related to
 *      the use of the provided software.
 *
 * User: hugues
 * Date: 26/10/2015
 */

namespace LinxoClient\client\actions\auth;

include_once(__DIR__.'/../LinxoAction.php');
use LinxoClient\client\actions\LinxoAction;

include_once(__DIR__.'/../../dto/device/AppInfo.php');
use LinxoClient\client\dto\device\AppInfo;

/**
 * Class AuthorizeDeviceAction
 *
 * Authenticates a device with the email/password and then returns a token associated
 * with the deviceId for future authenticates
 * @see LinxoClient\client\actions\auth\LoginDevice
 * @see LinxoClient\client\actions\auth\AuthorizeDeviceResult
 *
 * @package LinxoClient\client\actions\auth
 */
class AuthorizeDeviceAction extends LinxoAction
{
  var $identifier;
  var $password;

  var $deviceFamily;
  var $deviceType;
  var $deviceId;
  var $deviceName;

  /**
   * @type AppInfo
   */
  var $appInfo;

}