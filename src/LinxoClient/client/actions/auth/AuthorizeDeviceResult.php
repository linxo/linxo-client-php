<?php
/**
 *  Copyright (c) 2015-2015 Linxo, All Rights Reserved.
 *
 * COPYRIGHT:
 *      This software is the property of Linxo.
 *      It cannot be copied, used, or modified without obtaining an
 *      authorization from the authors or a person mandated by Linxo.
 *      If such an authorization is provided, any modified version
 *      or copy of the software has to contain this header.
 *
 * WARRANTIES:
 *      This software is made available by the authors in the hope
 *      that it will be useful, but without any warranty.
 *      Linxo is not liable for any consequence related to
 *      the use of the provided software.
 *
 * User: hugues
 * Date: 26/10/2015
 */

namespace LinxoClient\client\actions\auth;

include_once(__DIR__.'/../linxoResult.php');
use LinxoClient\client\actions\LinxoResult;

include_once(__DIR__.'/../../dto/device/AppInfo.php');
use LinxoClient\client\data\auth\AuthStatus;
use LinxoClient\client\dto\device\AppInfo;

include_once(__DIR__.'/../../dto/user/DealInfo.php');
use LinxoClient\client\dto\user\DealInfo;

require_once ( __DIR__. "/../../data/permissions/Feature.php");

include_once(__DIR__.'/../../dto/user/UserProfileInfo.php');
use LinxoClient\client\dto\user\PermissionInfo;
use LinxoClient\client\dto\user\UserProfileInfo;



class AuthorizeDeviceResult extends LinxoResult
{
  var $token;
  var $blocked;
  var $blockedAddress;
  var $firstName;
  var $lastName;
  var $isActivated;
  var $askForTermsAndConditions;

  /**
   * enum listed in \LinxoClient\client\data\auth\AuthStatus
   */
  var $status;

  /**
   * The current deal for this user
   * @type DealInfo
   */
  var $dealInfo;

  /**
   * The current permissions for this user
   * @array of PermissionInfo objects
   */
  var $permissions;

  /**
   * The appInfo
   * @type AppInfo
   */
  var $appInfo;

  /**
   * The profile infor for this user
   * @type UserProfileInfo
   */
  var $userProfileInfo;

  /**
   * AuthorizeDeviceResult constructor.
   * @param \stdClass $that
   */
  public function __construct(\stdClass $that)
  {
    parent::__construct($that);

    $this->token = $that->token;
    $this->blocked = $that->blocked;
    $this->blockedAddress = $that->blockedAddress;
    $this->firstName = $that->firstName;
    $this->lastName = $that->lastName;
    $this->isActivated = $that->isActivated;
    $this->askForTermsAndConditions = $that->askForTermsAndConditions;

    $this->status = AuthStatus::valueOf($that->status);

    if($that->dealInfo != null){
      $this->dealInfo = new DealInfo($that->dealInfo);
    }

    $this->permissions = array();
    foreach($that->permissions as $stdPermission )
    {
      $this->permissions[] = new PermissionInfo($stdPermission);
    }

    if($that->appInfo != null){
      $this->appInfo = new AppInfo(
          $that->appInfo->appIdentifier,
          $that->appInfo->appVersion,
          $that->appInfo->upgradeRequired);
    }

    if($that->userProfileInfo != null) {
      $this->userProfileInfo = new UserProfileInfo($that->userProfileInfo);
    }

  }


}