<?php
/**
 *  Copyright (c) 2015-2015 Linxo, All Rights Reserved.
 *
 * COPYRIGHT:
 *      This software is the property of Linxo.
 *      It cannot be copied, used, or modified without obtaining an
 *      authorization from the authors or a person mandated by Linxo.
 *      If such an authorization is provided, any modified version
 *      or copy of the software has to contain this header.
 *
 * WARRANTIES:
 *      This software is made available by the authors in the hope
 *      that it will be useful, but without any warranty.
 *      Linxo is not liable for any consequence related to
 *      the use of the provided software.
 *
 * User: hugues
 * Date: 26/10/2015
 */

namespace LinxoClient\client\actions\auth\signup;

require_once(__DIR__.'/../../../../infrastructure/BasicEnum.php');
use LinxoClient\infrastructure\BasicEnum;

abstract class SignUpResultStatus extends BasicEnum
{
  const Success                   = 'Success'; // what is not Success is an error
  const EmailAlreadyExistFailure  = 'EmailAlreadyExistFailure';
  const FieldFailure              = 'FieldFailure'; // see list of fields
  const GeneralFailure            = 'GeneralFailure';
}


abstract class SignUpResultField extends BasicEnum
{
  const FirstName                 = 'FirstName';
  const LastName                  = 'LastName';
  const Email                     = 'Email';
  const Password                  = 'Password';
  const Question                  = 'Question';
  const Answer                    = 'Answer';
  const ZipCode                   = 'ZipCode';
  const AcceptTermsAndConditions  = 'AcceptTermsAndConditions';
  const SponsorCode               = 'SponsorCode'; // the provided sponsor code is not in the DB
//    AcceptPartnersOffers, // not an error in the fields
  }



require_once(__DIR__.'/../../LinxoResult.php');
use LinxoClient\client\actions\LinxoResult;

class SignUpResult extends LinxoResult
{
  /**
   * @type SignUpResultStatus
   * status of the signUp
   */
  var $status;

  /**
   * @type SignUpResultField
   * @type array of fields in error
   */
  var $fields;

  /**
   * SignUpResult constructor.
   * @param \stdClass $that
   */
  public function __construct(\stdClass $that)
  {
    parent::__construct($that);

    $this->status = SignUpResultStatus::valueOf($that->status);

    $this->fields = array();
    foreach($that->fields as $field)
    {
      $this->fields[] = SignUpResultField::valueOf($field);
    }
  }
}

