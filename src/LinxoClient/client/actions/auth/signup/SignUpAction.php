<?php
/**
 *  Copyright (c) 2015-2015 Linxo, All Rights Reserved.
 *
 * COPYRIGHT:
 *      This software is the property of Linxo.
 *      It cannot be copied, used, or modified without obtaining an
 *      authorization from the authors or a person mandated by Linxo.
 *      If such an authorization is provided, any modified version
 *      or copy of the software has to contain this header.
 *
 * WARRANTIES:
 *      This software is made available by the authors in the hope
 *      that it will be useful, but without any warranty.
 *      Linxo is not liable for any consequence related to
 *      the use of the provided software.
 *
 * User: hugues
 * Date: 26/10/2015
 */

namespace LinxoClient\client\actions\auth\signup;

include_once(__DIR__.'../../LinxoAction.php');
use LinxoClient\client\actions\LinxoAction;

/**
 * This action and its result allows the user to sign up in a single pass.
 * It is intended to be used through an API signed call
 */
class SignUpAction extends LinxoAction
{
  var $firstName;
  var $lastName;
  var $email;
  var $password ;
  var $question;
  var $answer;
  var $zipCode;
  var $acceptPartnerOffers;
  var $acceptTermsAndConditions;
  var $sponsorCode;

  /**
   * SignUpAction constructor.
   * @param $firstName
   * @param $lastName
   * @param $email - required
   * @param $password - required
   * @param $question
   * @param $answer
   * @param $zipCode
   * @param $acceptPartnerOffers
   * @param $acceptTermsAndConditions
   * @param $sponsorCode
   */
  public function __construct($firstName, $lastName, $email, $password, $question, $answer, $zipCode, $acceptPartnerOffers, $acceptTermsAndConditions, $sponsorCode)
  {
    $this->firstName = $firstName;
    $this->lastName = $lastName;
    $this->email = $email;
    $this->password = $password;
    $this->question = $question;
    $this->answer = $answer;
    $this->zipCode = $zipCode;
    $this->acceptPartnerOffers = $acceptPartnerOffers;
    $this->acceptTermsAndConditions = $acceptTermsAndConditions;
    $this->sponsorCode = $sponsorCode;
  }


}