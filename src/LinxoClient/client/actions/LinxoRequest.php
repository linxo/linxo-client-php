<?php
/*
   Copyright (c) 2015-2015 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
 */
namespace LinxoClient\client\actions;

include_once("Hash.php");
include_once("LinxoAction.php");



/**
 */
class LinxoRequest
{
  var $actionName;
  var $action;
  var $hash;

  //o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-
  //
  //      CONSTRUCTORS
  //
  //o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-


  public function __construct($action, $actionName, $hash)
  {
    $this->action = $action;
    $this->actionName = $actionName;
    $this->hash = $hash;
  }

  //o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-
  //
  //      PUBLIC API
  //
  //o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-

  /**
   * @return mixed
   */
  public function getActionName()
  {
    return $this->actionName;
  }

  /**
   * @return mixed
   */
  public function getAction()
  {
    return $this->action;
  }

  /**
   * @return mixed
   */
  public function getHash()
  {
    return $this->hash;
  }

}