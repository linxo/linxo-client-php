<?php
/**
 *  Copyright (c) 2015-2015 Linxo, All Rights Reserved.
 *
 * COPYRIGHT:
 *      This software is the property of Linxo.
 *      It cannot be copied, used, or modified without obtaining an
 *      authorization from the authors or a person mandated by Linxo.
 *      If such an authorization is provided, any modified version
 *      or copy of the software has to contain this header.
 *
 * WARRANTIES:
 *      This software is made available by the authors in the hope
 *      that it will be useful, but without any warranty.
 *      Linxo is not liable for any consequence related to
 *      the use of the provided software.
 *
 * User: hugues
 * Date: 27/10/2015
 */

namespace LinxoClient\client\actions;

require_once(__DIR__.'/LinxoResult.php');
require_once(__DIR__.'/../../infrastructure/BasicEnum.php');
use LinxoClient\infrastructure\BasicEnum;

abstract class ErrorCode extends BasicEnum {
  const ErrUnknown            ='ErrUnknown';            // Unknown error
  const ErrBadSignature       ='ErrBadSignature';       // The device sent an invalid signature
  const ErrInvalidSession     ='ErrInvalidSession';     // The device needs to login, the previous session has timed out.
  const ErrInvalidSecret      ='ErrInvalidSecret';      // The device sent a null or invalid secret.
  const ErrActionFormat       ='ErrActionFormat';       // We've got a problem while parsing the action.
  const ErrInvalidTimeStamp   ='ErrInvalidTimeStamp';   // The timestamp is not within accepted bounds @see ApiConfiguration#timestampCheckMargin
  const ErrNoHandler          ='ErrNoHandler';          // We've got no handler for such an action. This is a BIG bug !
  const ErrPermissionDenied   ='ErrPermissionDenied';   // User need to subscribe a better deal to access this feature
}


class ErrorResult extends LinxoResult
{
  var $errorCode;

  var $errorMessage;

  var $failedAction;


  public function __construct(\stdClass $that)
  {
    parent::__construct($that);

    $this->errorCode = ErrorCode::valueOf($that->errorCode);

    $this->errorMessage = $that->errorMessage;
    $this->failedAction = $that->failedAction;
  }
}