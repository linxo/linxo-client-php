<?php
/**
 *  Copyright (c) 2015-2015 Linxo, All Rights Reserved.
 *
 * COPYRIGHT:
 *      This software is the property of Linxo.
 *      It cannot be copied, used, or modified without obtaining an
 *      authorization from the authors or a person mandated by Linxo.
 *      If such an authorization is provided, any modified version
 *      or copy of the software has to contain this header.
 *
 * WARRANTIES:
 *      This software is made available by the authors in the hope
 *      that it will be useful, but without any warranty.
 *      Linxo is not liable for any consequence related to
 *      the use of the provided software.
 *
 * User: hugues
 * Date: 03/11/2015
 */

namespace LinxoClient\client\actions\user;

require_once(__DIR__.'/../LinxoResult.php');
require_once(__DIR__.'/../../dto/user/PermissionInfo.php');

use LinxoClient\client\actions\LinxoResult;
use LinxoClient\client\dto\user\PermissionInfo;

class GetPermissionsResult extends LinxoResult
{
  /** @type array */
  var $permissions;

  /**
   * GetPermissionsResult constructor.
   * @param \stdClass $that
   */
  public function __construct(\stdClass $that)
  {
    parent::__construct($that);

    $this->permissions = array();
    if($that->permissions  != null) {
      foreach($that->permissions  as $stdPermission) {
        $this->permissions [] = new PermissionInfo($stdPermission);
      }
    }

  }


}