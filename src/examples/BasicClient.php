<?php
namespace examples;

require_once(__DIR__.'/../LinxoClient/client/net/LinxoClient.php');
use LinxoClient\client\actions\ErrorResult;
use LinxoClient\client\net\LinxoClient;

require_once(__DIR__.'/../LinxoClient/client/actions/auth/LoginAction.php');
use LinxoClient\client\actions\auth\LoginAction;

require_once(__DIR__.'/../LinxoClient/client/actions/pfm/GetAccountGroupsAction.php');
use LinxoClient\client\actions\pfm\GetAccountGroupsAction;



echo "Creating LinxoClient\n";

// TODO : REPLACE WITH API KEY and API SECRET
$linxoClient = new LinxoClient("partners.linxo.com", "API KEY" , "API SECRET");

echo "LinxoClient:".json_encode($linxoClient)."\n";

$linxoClient->initialize();

echo "LinxoClient (initialized):".json_encode($linxoClient)."\n";

// TODO : replace with login and password
$loginResult = $linxoClient->sendAction(new LoginAction("LOGIN", "PASSWORD"));

echo "LoginResult[".json_encode($loginResult)."]\n";

if ( $loginResult->userId == NULL )
{
  die ("Error");
}

echo "LinxoClient (after login):".json_encode($linxoClient)."\n";

$accountGroupResult = $linxoClient->sendAction(
    new GetAccountGroupsAction()
);

echo "GetAccountGroupsResult[".json_encode($accountGroupResult)."]\n";

require_once(__DIR__.'/../LinxoClient/client/actions/pfm/GetTransactionsAction.php');
require_once(__DIR__.'/../LinxoClient/client/actions/user/GetPermissionsAction.php');
use LinxoClient\client\actions\pfm\GetTransactionsAction;
use LinxoClient\client\actions\user\GetPermissionsAction;

$getPermissionsAction = new GetPermissionsAction();
echo "GetPermissionsAction [".json_encode($getPermissionsAction, JSON_PRETTY_PRINT)."]\n";

$getPermissionsResult = $linxoClient->sendAction(
    $getPermissionsAction
);
echo "GetPermissionsResult[".json_encode($getPermissionsResult, JSON_PRETTY_PRINT)."]\n";


$getTransactionsAction = new GetTransactionsAction(0, 10);
echo "GetTransactionsAction[".json_encode($getTransactionsAction, JSON_PRETTY_PRINT)."]\n";

$getTransactionsResult = $linxoClient->sendAction(
    $getTransactionsAction
);

echo "GetTransactionsResult[".json_encode($getTransactionsResult, JSON_PRETTY_PRINT)."]\n";

require_once(__DIR__.'/../LinxoClient/client/actions/pfm/GetAlertsAction.php');
use LinxoClient\client\actions\pfm\GetAlertsAction;

$getAlertsAction = new GetAlertsAction();
echo "GetAlertsAction[".json_encode($getAlertsAction, JSON_PRETTY_PRINT)."]\n";

$getAlertsResult = $linxoClient->sendAction(
    $getAlertsAction
);

echo "GetAlertsResult[".json_encode($getAlertsResult, JSON_PRETTY_PRINT)."]\n";


// query linxoBank
require_once(__DIR__.'/../LinxoClient/client/actions/pfm/sync/GetFinancialInstitutionAction.php');
use LinxoClient\client\actions\pfm\sync\GetFinancialInstitutionAction;

$fid = 87; // Linxo Bank
$getFinancialInstitutionResult = $linxoClient->sendAction(
    new GetFinancialInstitutionAction($fid)
);

echo ("LinxoClient got financial institution [".$fid."]: [".
    json_encode($getFinancialInstitutionResult->financialInstitution)."] \n");

$financialInstitution = $getFinancialInstitutionResult->financialInstitution;

foreach($financialInstitution->keys as $key) {
  echo "Key(".$key->key."):" . $key->label ." will accept values in range [".$key->regexp."]\n";
  if( strstr($key->key, "secret") ) {
    echo ">> Key(".$key->key.") must be encrypted.\n";
  }
}

// Register to events
echo "LinxoClient registers to events\n";

require_once(__DIR__.'/../LinxoClient/client/actions/events/EventRegisterAction.php');
use LinxoClient\client\actions\events\EventRegisterAction;

$eventRegisterResult = $linxoClient->sendAction(
    new EventRegisterAction()
);

if($eventRegisterResult->queueId == null) {
  die ("Error:[".json_encode($eventRegisterResult)."]");
}
echo ("LinxoClient registers to queueId [".$eventRegisterResult->queueId."] \n");

$queueId = $eventRegisterResult->queueId;

require_once(__DIR__.'/../LinxoClient/client/actions/events/GetNextEventsAction.php');
use LinxoClient\client\actions\events\GetNextEventsAction;

$nextEventsResult = $linxoClient->sendAction(
    new GetNextEventsAction($queueId, 0)
);

echo ("Latest events[".$nextEventsResult->events."] \n");


//$gpg = new \gnupg();
//$gpg->addencryptkey($financialInstitution->publicKey);


// Create credentials objects and populate the Action to launch the synchronization

require_once(__DIR__.'/../LinxoClient/client/actions/pfm/sync/Credential.php');
use LinxoClient\client\actions\pfm\sync\Credential;

$credentials = array();
$credentials[] = new Credential(null, "credentials.linxo.identifier", "dev1@linxo.com");
$credentials[] = new Credential(null, "credentials.linxo.secret", "gpg(\$this->pubKey, password1)");
$credentials[] = new Credential(null, "credentials.linxo.home_url", "/opt/linxo/bank/linxo-bank.1.txt");

require_once(__DIR__.'/../LinxoClient/client/actions/pfm/sync/ListAccountsInFinancialInstitutionAction.php');
use LinxoClient\client\actions\pfm\sync\ListAccountsInFinancialInstitutionAction;
require_once(__DIR__.'/../LinxoClient/client/actions/pfm/events/AccountListUpdateEvent.php');
use LinxoClient\clients\action\pfm\events\AccountListUpdateEvent;
use LinxoClient\clients\action\pfm\events\AccountListUpdateEventUpdateType;

$listAccountsInFinancialInstitutionResult = $linxoClient->sendAction(
    new ListAccountsInFinancialInstitutionAction($fid, $credentials)
);

echo "listAccountsInFinancialInstitutionResult[".json_encode($listAccountsInFinancialInstitutionResult)."]";
if($listAccountsInFinancialInstitutionResult instanceof ErrorResult) {
  die ("Cannot launch synchronization [".json_encode($listAccountsInFinancialInstitutionResult)."]");
}

$lastEventNumber = 0;
$providerAccounts = null;
$finished = false;
while(! $finished) {
  echo "querying events lastEventNo[$lastEventNumber]\n";

  $nextEventsResult = $linxoClient->sendAction(
      new GetNextEventsAction($queueId, $lastEventNumber)
  );

  echo "queried events lastEventNo[" . @json_encode($nextEventsResult) . "]\n";
  if ($nextEventsResult->events != null) {
    echo "No event lastEventNo[" . @json_encode($nextEventsResult) . "]\n";
    sleep(2);
  }

  foreach ($nextEventsResult->events as $event) {
    /** @noinspection PhpUndefinedMethodInspection */
    $lastEventNumber = $event->getEventNumer();

    echo "Event [" . json_encode($event) . "]\n";

    if (!($event instanceof AccountListUpdateEvent)) {
      continue;
    }

    // $event is a 'AccountListUpdateEvent'
    switch ($event->updateType) {
      case AccountListUpdateEventUpdateType::Update : {
        continue;
      }

      case AccountListUpdateEventUpdateType::Failure: {
        // in case of failure, we stop.
        die ("Could not link the accounts : error while connecting [" . json_encode($event) . "]\n");
      }

      case AccountListUpdateEventUpdateType::Success: {
        // In case of success, we add all the found accounts
        echo "Got providerAccounts[" . json_encode($event->providerAccounts) . "]\n";
        $providerAccounts = $event->providerAccounts;
        $finished = true;
      }
    }
  }
}

if($providerAccounts == null) {
  die("No account found");
}

require_once(__DIR__.'/../LinxoClient/client/actions/pfm/sync/SelectAccountsForSynchronizationAction.php');
use LinxoClient\client\actions\pfm\sync\SelectAccountsForSynchronizationAction;

$selectAccountsForSynchronizationResult = $linxoClient->sendAction(
    new SelectAccountsForSynchronizationAction($providerAccounts)
);




// TODO : launch an event loop to listen to SynchronizationUpdateEvent to verify the  end of the synch.
// TODO : then list the groups and transactions

